package info.inetsolv.app;

import org.springframework.boot.context.properties.ConfigurationProperties;
//@org.springframework.context.annotation
@ConfigurationProperties(prefix = "db")
public class Configuration {
private String driverClass;
private String url;
private String userName;
private String password;
}
